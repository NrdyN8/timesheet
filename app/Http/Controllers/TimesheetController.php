<?php

namespace App\Http\Controllers;

use DateTime;
use DatePeriod;
use DateInterval;

use App\Timesheet;
use Illuminate\Http\Request;

class TimesheetController extends Controller
{
      /**
       * Create a new time sheet controller instance.
       *
       * Sets this controller to authorized only users.
       *
       * @return void
       */
      public function __construct(){
        $this->middleware('auth');
        $this->date = ["year"  => Date('Y'),
                       "month" => Date('m'),
                       "day"   => Date('d')];
        $this->week_num = idate('W', time());
        //$this->df = 'Y-m-d'; //Date format
      }
      //Get
      //uri:/time
      public function index(){
        return view('timesheet.index', ["date" => $this->date,
                                        "week" => $this->getWeek($this->week_num, $this->date['year'])
                                      ]);
      }
      public function updateWeek(){
      }
      //Get
      //uri:/time/create
      public function create(){

      }
      //Post
      //uri:/time
      public function store(){

      }
      //Get
      //uri:/time/{id}
      public function show(){

      }
      //Get
      //uri:/time/{id}/edit
      public function edit(){

      }
      //Put/Patch
      //uri:/time/{id}
      public function update(){

      }
      //Delete
      //uri:/time/{id}
      public function destroy(){

      }

      private function getWeek($week, $year){
        $dto = new DateTime();
        $week_start = $dto->setISODate($year, $week)->getTimeStamp();
        $week_end = $dto->modify('+6 days')->getTimeStamp();
        $week_dates = array();
        array_push($week_dates,$this->getDayArray($week_start)); //Adds first day of the week
        while ($week_start<$week_end)
        {
            $week_start+=86400; // add 24 hours
            array_push($week_dates,$this->getDayArray($week_start));
        }

        return $week_dates;
      }
      private function getDayArray($day){
        return ['year'     => date('Y', $day),
                'month'    => date('m', $day),
                'day'      => date('d', $day),
                'day_3'    => date('D', $day),
                'day_full' => date('l', $day)];
      }
  }
