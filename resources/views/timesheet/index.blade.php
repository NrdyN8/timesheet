@extends('layouts.app')

@section('content')
  <table class="time_table">
    @foreach ($week as $day)
      <tr>
        {{$day['day_3']}} - {{$day['day']}}
      </tr>
    @endforeach
  </table>
@endsection
